# Flask
[参考示例](http://www.cnblogs.com/wupeiqi/articles/7552008.html)

```
pip3 install flask
```

- Flask [Hello World!](/hello.py)
- [socket服务端](#socket服务端)
- [配置](#配置)
- [路由](#路由)
- [请求与响应](#请求与响应)
- [cookie](#cookie)
- [session](#session)
- [蓝图](#蓝图)
- [message](#message)
- [中间件](#中间件)
- [Flask插件](#插件)

## socket服务端

```
wsgi:Web服务网关接口
    - wsgiref(django)
    - werkzeug(flask) 安装完Flask后默认安装上了
```
- werkzeug(flask)

[werkzeug 代码示例](/werkzeug.py)

```python
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

@Request.application
def hello(request):# 回调函数
	return Response('Hello World!')

if __name__ == '__main__':
	run_simple('localhost', 4000, hello)   
	# 第三个参数的解释:
	# 如果传的是函数就执行这个回调函数,
	# 如果传的是对象,就执行它类的 __call__ 方法
```
- wsgiref(django)

django里面的wsgiref原理[wsgiref 代码示例](/wsgiref.py)

## 配置
- [静态文件url](#static_url_path)
- [返回模板(html)](#template_folder)
- [其它配置](#其它配置)

[demo](/flask_project)

```
Flask
├── flask_project.py
├── settings.py
├── static
│   └── 1.JPG   // 默认: 127.0.0.1:5000/static/1.jpg
└── templates
    └── hello.html
```

```python
import_name, 
static_url_path=None,          # 静态前缀：/sssss /开头
static_folder='static',        # 静态文件路径
template_folder='templates',   # 模板路径
# 后面的可能用不到
instance_path=None,            # stance  app根目录/instance
instance_relative_config=False,# 为 True 的时候上面那一条配置才有用,去那个目录下找配置文件
root_path=None                 # app根目录下找配置文件
```
### static_url_path
静态文件url

```python
# 127.0.0.1:5000/ss/1.jpg
app = Flask(__name__,static_url_path='/ss')   # 创建App   创建一个Flask对象
```

### template_folder
templates 下面创建html文件,返回模板(html)

```python
# 访问地址: 127.0.0.1:5000/
# 导入
from flask import render_template

# 视图函数
@app.route('/')     # 注册路由
def hello_world():
    # return 'Hello World!'
    return render_template('hello.html')
```

### 其它配置
 配置文件写`KEY`的时候不能是小写,一定要大写
 
 **导入配置的两种方式:**
 
 - [method 1](#方式1)
 - [method 2](#方式2)
 
#### 方式1

```python
app.config['DEBUG'] = True
app.debug = True
app.session_interface
app.config.update({})
```
#### 方式2
- [从文件导入](#从文件导入)
- [环境变量](#去环境变量取)
- [JSON文件](#JSON文件导入)
- [字典](#字典导入)
- [通过对象导入(推荐)](#通过对象来导入)

#### 从文件导入

app.config.from_pyfile("python文件名称")
[文件示例](/flask_project/settings.py)

```python
app.config.from_pyfile("settings.py")
```

#### 去环境变量取

app.config.from_envvar('环境变量')
[文件示例](/flask_project/test.py)

```python
# 调用流程
app.config.from_envvar('xxxxx')  # ⥥⥥⥥ 从环境变量里面取  ⬇︎⬇︎⬇︎
# 最终还是以文件导入的方式
app.config.from_pyfile(app.config.from_envvar('xxxxx'))
```
#### JSON文件导入

把JSON文件名写在这儿即可

```python
app.config.from_json('oo.json') # 从JSON文件读取
``` 
#### 字典导入

```python
app.config.from_mapping({'DEBUG':True}) # 字典导入
```

#### 通过对象来导入
**推荐使用**
app.config.from_object("python类或类的路径")
[文件示例](/flask_project/settings.py)

```python
app.config.from_object('settings.DevelopmentConfig')    # 基于对象来导入
```

```python
# settings.py
class Config(object):
    DEBUG = False
    TESTING = False
    DATABASE_URI = 'sqlite://:memory:'
 
class ProductionConfig(Config):
    DATABASE_URI = 'mysql://user@localhost/foo'
 
class DevelopmentConfig(Config):
    DEBUG = True
 
class TestingConfig(Config):
    TESTING = True  # 继承 Config 全都应用上了
```
PS: 从sys.path中已经存在路径开始写

## 路由
- [反向生成url](#不用装饰器)
- [常用的路由](#常用的路由)
- [自定义正则](#自定义正则)
- [注册路由原理](#注册路由原理)

### 常用的路由

- @app.route('/user/<username>')
- @app.route('/post/<int:post_id>')
- @app.route('/post/<float:post_id>')
- @app.route('/post/<path:path>')
- @app.route('/login', methods=['GET', 'POST'])

```python
@app.route('/edit/<int:nid>')
def hello_world(nid):
    print('nid: %s'%(nid))
    return 'Hello World!'
```
常用路由系统有以上五种，所有的路由系统都是基于一下对应关系来处理：

```python
DEFAULT_CONVERTERS = {
    'default':          UnicodeConverter,
    'string':           UnicodeConverter,
    'any':              AnyConverter,
    'path':             PathConverter,
    'int':              IntegerConverter,
    'float':            FloatConverter,
    'uuid':             UUIDConverter,
}
```

### 不用装饰器
view_func: 对应的函数
endpoint: 反向生成url(同Django中的name)
methods: GET/POST(这个函数支持的请求方式)

```python
def hello_world():
    from flask import url_for   # 反向生成url模块
    url = url_for('xxx')    # 反向生成url
    return 'Hello World!'

app.add_url_rule('/',view_func=hello_world,endpoint='xxx',methods=["GET","POST"])
```
### 自定义正则
[自定义路由demo](/reurl.py)

```python
# 注册
app.url_map.converters['regex'] = RegexConverter

# 示例
@app.route('/index/<regex("\d+"):nid>')
def index(nid):
    print(url_for('index', nid='888'))
    return 'Index'
```

### 注册路由原理
[传送门](/http://www.cnblogs.com/wupeiqi/articles/7552008.html)

## 模板
- 模板使用

与Django没有差别

- 自定义模板方法

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <h1>自定义函数</h1>
    {{ww()|safe}}

</body>
</html>
```

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask,render_template
app = Flask(__name__)
 
 
def wupeiqi():
    return '<h1>Wupeiqi</h1>'
 
@app.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('login.html', ww=wupeiqi)    # ww就是自定义的方式
 
app.run()
```

## 请求与响应

请求在flask里面大部分功能都是通过导入之后来直接进行操作,request 的http请求 `app.run` > `run_simple` >
 `__call __` > `self.wsgi_app` > `start_response`
 
```
# 对请求数据进行处理,让它变成一个对象
ctx = self.request_context(environ) 
ctx.push()  # 把这个request对象拿过来,再创建一个对象,然后放到from 导入那里,相当于一个全局变量
# 其实就是动态的创建reqeust对象,放到全局调用
```
 
 如:
[更多方法传送门](/request.md)

```python
from flask import request   
# 这里是一个全局变量的功能,flask会实时刷新
# 谁访问,当前就是调用谁的request

@app.route('/login.html', methods=['GET', "POST"])
    def login():
        print(request.method)   # 这里直接用,request是全局的
        ...
        return 'xxxx'
```

## cookie
操作 `cookie`

```python
from flask import make_response

@app.route('/', methods=['GET', "POST"])
def test()
    # 先把返回的数据封装一下,就是为了回写cookie等其它操作
    response = make_response(render_template('index.html'))
    # response是flask.wrappers.Response类型
    # 封装完成以后就可以操作cookie了
    response.delete_cookie('key')   # 删除 
    response.set_cookie('key', 'value') # 设置cookie
    response.headers['X-Something'] = 'A value'     # 响应头
    return response # 返回数据
```

## session
除[请求对象](#请求与响应)之外，还有一个 session 对象。它允许你在不同请求间存储特定用户的信息。它是在 Cookies 的基础上实现的，并且对 Cookies 进行密钥签名要使用会话，需要设置一个密钥。

- 默认session放在加密的cookie中
- 改造:必须有open...save...两个方法  下面代码

```python
# 必须继承这两个类
class MySession(dict, SessionMixin)
	pass

class MySessionInterface(SessionInterface):
	def open_session(self, app, request)
		return MySession()
	def save_session(self, app, session, response)


app.session_interface = MySessionInterface()
```

```python
app.secret_key = 'asdfasdfdf' # 设置的密钥

# 导入
from flask.sessions import SecureCookieSessionInterface

@app.route('/', methods=['GET', "POST"])
def test()
    # 获取用户请求数据
    session['user'] = 'ceo'
    session['age'] = 37
```

在flask中使用自定义session
[示例](/sessions.py)

```python
# 先导入你写的那个类
from sessions import MySessionInterface
# 这里设置为自定义session的类,就不使用内置的session了
app.session_interface = MySessionInterface()
```

ps:也有现成的插件`pip3 install Flask-Session`

## 蓝图

上面的demo都是单个py文件,如果是大项目的话单个py文件就不现实


- 自己创建目录: [demo](/demo2)

利用导入包的原理来实现

```
./demo2
├── demo
│   ├── __init__.py
│   └── views
│       ├── account.py
│       └── host.py
└── run.py
```

- 蓝图[示例 demo](/demo)
    - 单独的蓝图也可以有自己的静态目录(用的不多)
    - url_prefix : 当前蓝图url固定前缀

```
./demo
├── demo
│   ├── __init__.py
│   └── views
│       ├── account.py
│       └── host.py
├── run.py
├── static
└── templates
```

```python
# 逻辑文件.py
# step 1
from flask import Blueprint
# step 2
account = Blueprint('account',__name__,url_prefix='/xxx') # 名字是没所谓的
# step 3
@account.route('/login.html')
def login():
    # return render_template('login.html')
    return 'Login'

# step 4
# __init__.py
from flask import Flask,render_template

app = Flask(__name__)

from .views.account import account
from .views.host import host

app.register_blueprint(account) # 注册
app.register_blueprint(host)    # 注册
```

## message
其特点是：**使用一次就删除**。
message是一个基于Session实现的用于保存数据的集合

应用场景:

- 错误信息
- A页面传数据到B页面

[示例 code](/message.py)

## 中间件
[示例 code](/md.py)

请求来了会执行`__call__`方法
自定义类,重写 `__call__`

## 插件
- Flask-Session***
- WTForms***
- SQLAchemy
- 等...    http://flask.pocoo.org/extensions/



from flask import Flask,render_template

app = Flask(__name__)


from .views.account import account
from .views.host import host

app.register_blueprint(account)
app.register_blueprint(host)
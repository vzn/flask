from flask import Flask,render_template,make_response,session
from sessions import MySessionInterface

app = Flask(__name__,static_url_path='/ss')   # 创建App   创建一个Flask对象
app.session_interface = MySessionInterface()

@app.route('/')     # 注册路由
def hello_world():
    # return 'Hello World!'
    return render_template('hello.html')

# 配置
# 方式 1
# app.config['DEBUG'] = True
# app.debug = True
# app.session_interface
# app.config.update({})
# 方式 2  ...
# app.config.from_pyfile("settings.py")   # 从文件导入
# app.config.from_envvar('环境变量')  # 从环境变量里面取
# app.config.from_json('oo.json') # 从JSON文件读取
# app.config.from_mapping({'DEBUG':True}) # 字典导入
# app.config.from_object('settings.TestingConfig')    # 基于对象来导入
# PS: 从sys.path中已经存在路径开始写

@app.route('/sess')
def sess():
    pass

if __name__ == '__main__':
    app.run()   # 运行    @ 请求入口

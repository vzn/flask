from flask import Flask

app = Flask(__name__)   # 创建App   创建一个Flask对象


@app.route('/')     # 注册路由
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run()   # 运行    @ 请求入口

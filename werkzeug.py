from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

@Request.application
def hello(request):# 回调函数
    return Response('Hello World!')

if __name__ == '__main__':
    run_simple('localhost', 4000, hello)
    # 第三个参数的解释:
    # 如果传的是函数就执行这个回调函数,
    # 如果传的是对象,就执行它类的 __call__ 方法
from flask import Flask, flash, redirect, render_template, request

app = Flask(__name__)
app.secret_key = 'some_secret'


@app.route('/index')
def index():
    return 'index'


class MiddleWare:
    def __init__(self, wsgi_app):
        self.wsgi_app = wsgi_app

    def __call__(self, *args, **kwargs):
        print('before')
        response = self.wsgi_app(*args,**kwargs)
        # return self.wsgi_app(*args, **kwargs)
        print('after')
        return response


if __name__ == "__main__":
    app.wsgi_app = MiddleWare(app.wsgi_app)
    app.run(port=9999)
